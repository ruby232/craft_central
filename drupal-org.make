; Drupal.org releases file
core = 7.x
api = 2

; Defaults
defaults[projects][subdir] = "contrib"

; Module
projects[admin_menu][version] = 3.0-rc5
projects[backup_migrate][version] = 2.8
projects[better_exposed_filters][version] = 3.0-beta4
projects[context][version] = 3.7
projects[ctools][version] = 1.14
projects[date][version] = 2.10
projects[devel][version] = 1.6
projects[features][version] = 2.10
projects[features_override][version] = 2.0-rc3
projects[file_entity][version] = 2.21
projects[filefield_paths][version] = 1.0
projects[filter_perms][version] = 1.0
projects[jquery_update][version] = 2.7
projects[libraries][version] = 2.3
projects[module_filter][version] = 2.1
projects[pathauto][version] = 1.3
projects[scroll_to_top][version] = 2.1
projects[strongarm][version] = 2.0
projects[token][version] = 1.7
projects[variable][version] = 2.3
projects[views][version] = 3.20
projects[views_bootstrap][version] = 3.2
projects[views_bulk_operations][version] = 3.4
projects[less][version] = 4.0
projects[entity][version] = 1.9
projects[smart_trim][version] = 1.5
projects[views_field_view][version] = 1.2
projects[webform][version] = "4.17"
projects[options_element][version] = "1.12"
projects[bootstrap][version] = "3.20"
projects[realname][version] = "1.3"


projects[webform_civicrm][version] = "4.20"
projects[civicrm_realname][version] = "1.0"


libraries[civicrm][download][type] = get
#libraries[civicrm][download][url] = "http://downloads.civicrm.org/civicrm-4.4.7-starterkit.tgz"
libraries[civicrm][download][url] = "file:///var/app/libraries/civicrm-5.0.2-drupal.tar.gz"
libraries[civicrm][destination] = modules
libraries[civicrm][directory_name] = civicrm

libraries[lessphp][download][type] = "file"
libraries[lessphp][download][url] = "https://github.com/oyejorge/less.php/releases/download/v1.7.0.10/less.php_1.7.0.10.zip"


