<?php
/**
 * @file
 * User.
 */

/**
 * Create users and roles.
 */
function _craft_user_create() {
  _craft_user_create_superuser();
  _craft_user_set_roles_permissions();
}

/**
 * Crear el usuario con ID 1 y super administrator.
 */
function _craft_user_create_superuser() {
  // Create a default role for super administrator.
  $admin_role = new stdClass();
  $admin_role->name = 'super administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  variable_set('user_admin_role', $admin_role->rid);

  //Crear Super user
  $account = user_load(1);
  $data_account = [
    'name' => 'admin',
    'pass' => 'Admin123',
    'mail' => 'admin@craft.cu',
    'init' => 'admin@craft.cu',
    'roles' => [variable_get('user_admin_role')],
    'status' => 1,
    'timezone' => 'America/Havana',
  ];
  user_save($account, $data_account);
  //change pass for not espoused in code
  /*  db_update('users')
      ->fields(array('pass' => '$S$DBU5VhW2LHqQIggZ.o.F38x8yovetHmPBkLHzN6ZZFdzPivQPdFh'))
      ->condition('uid', 1)
      ->execute();*/
}

/**
 * Permission to roles
 */
function _craft_user_set_roles_permissions() {
  //Delete all permission
  db_delete('role_permission')
    ->execute();

  //anonymous users
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, _craft_user_anonimus_permissions());

  //autenticated users
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, _craft_user_autenticated_permissions());

  //all permissions to superadmin
  user_role_grant_permissions(variable_get('user_admin_role'), array_keys(module_invoke_all('permission')));

  $roles = _craft_user_get_roles();

  $modules = user_permission_get_modules();

  foreach ($roles as $role_name => $role_permissions) {
    $role = user_role_load_by_name($role_name);

    if (!$role) {
      //add rol if not exist
      $role = new stdClass();
      $role->name = $role_name;
      user_role_save($role);
      $role = user_role_load_by_name($role_name);
    }

    $permissions = [];
    // Check if exist permission.
    foreach ($role_permissions as $role_permission) {
      if (isset($modules[$role_permission])) {
        $permissions[] = $role_permission;
      }
      else {
        watchdog('craft_install', "Permission '$role_permission' not found in any module'.");
      }
    }

    user_role_grant_permissions($role->rid, $permissions);
  }

}

/**
 * Declare roles and permissions.
 */
function _craft_user_get_roles() {
  // Roles.
  $roles['administer'] = [];
  $roles['designer maker'] = [
    'access content',
    'create design content',
    'delete any design content',
    'delete own design content',
    'edit any design content',
    'edit own design content',
    'view own unpublished content',
  ];

  return $roles;

}

/**
 * Retorna los permisos del rol anonimus
 */
function _craft_user_anonimus_permissions() {
  return [
    'access content',
    'search content',
  ];
}

/**
 * Retorna los permisos del rol
 */
function _craft_user_autenticated_permissions() {
  return [
    'access content',
    'access user profiles',
  ];
}
