<?php
/**
 * @file
 * File menu for site.
 */

/**
 * Create menus.
 */
function _craft_menu_create() {
  watchdog("craft_install_menu", "Creating menus.");

  $user_menu = _craft_menu_user_menu();
  _craft_populate_menu($user_menu, 'user-menu');

  $main_menu = _craft_menu_main_menu();
  _craft_populate_menu($main_menu, 'main-menu');

  menu_cache_clear_all();

}

/**
 * Implements _craft_populate_menu().
 *
 * @param array $links
 *   Links.
 * @param string $menu_name
 *   Name.
 * @param int $plid
 *   Id link.
 */
function _craft_populate_menu($links, $menu_name, $plid = 0) {

  foreach ($links as $link) {
    $ls = array(
      'menu_name' => $menu_name,
      'link_title' => $link['link_title'],
      'description' => isset($link['description']) ? $link['description'] : 0,
      'link_path' => $link['link_path'],
      'weight' => isset($link['weight']) ? $link['weight'] : 0,
      'expanded' => isset($link['expanded']) ? $link['expanded'] : 0,
      'language' => isset($link['language']) ? $link['language'] : 'es',
      'plid' => $plid,
      'customized' => 1,
    );
    $pid = menu_link_save($ls);
    $link_title = $link['link_title'];
    watchdog("craft_install_menu", "Titulo $link_title, pid => $pid  ");
    if (!empty($link['childs'])) {
      _craft_populate_menu($link['childs'], $menu_name, $pid);
    }
  }
}

/**
 * Create Main Menu.
 *
 * @return array
 *   Menu.
 */
function _craft_menu_main_menu() {

  $main_menu = menu_load("main-menu");

  menu_save($main_menu);

  return [
    [
      'link_title' => st('My designs'),
      'link_path' => 'admin-designs',
      'weight' => -50
    ],
    [
      'link_title' => st('Designer Maker Directory'),
      'link_path' => 'designer-maker-directory',
      'weight' => -49
    ],
  ];
}

/**
 * User Menu.
 *
 * @return array
 *   Menu.
 */
function _craft_menu_user_menu() {

  $user_menu = menu_load('user-menu');
  menu_save($user_menu);

  return array(
    array(
      'link_title' => st('Login'),
      'link_path' => 'user/login',
      'weight' => -46,
    ),
  );
}
