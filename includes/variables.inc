<?php
/**
 * @file
 * File variable.inc.
 */

/**
 * set all variables.
 */
function _craft_install_variables() {
  _craft_variables_theme();
  _craft_variables_generales();
  _craft_variables_admin_menu();
  _craft_variables_pathauto();
}

/**
 * Variables generales del sitio.
 */
function _craft_variables_generales() {

  variable_set('site_name', "Craft Central");
  variable_set('site_mail', 'craft_central@gmail.com');
  variable_set('theme_default', 'craft_central');
  variable_set('clean_url', TRUE);


  // Record when this install ran.
  variable_set('install_time', $_SERVER['REQUEST_TIME']);

  // Zona horario y pais. ToDo
  //variable_set('date_default_timezone', 'America/Havana');
  //variable_set('site_default_country', 'CU');

  // Setear los jquery.
  variable_set('jquery_update_compression_type', "min");
  variable_set('jquery_update_jquery_cdn', "none");
  variable_set('jquery_update_jquery_migrate_cdn', "none");
  variable_set('jquery_update_jquery_migrate_enable', 0);
  variable_set('jquery_update_jquery_migrate_trace', 0);
  variable_set('jquery_update_jquery_migrate_warnings', 0);
  variable_set('jquery_update_jquery_version', "1.10");
  variable_set('jquery_update__active_tab', 'edit-jquery-migrate');

  // Allow visitor account creation with administrative approval. ToDo
  variable_set('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);
  // Usuario anonimo no tendra permisos a registrar nueva cuenta.
  //variable_set('user_register', '0');

  // alias url.
  variable_set('pathauto_reduce_ascii', '1');

  //Configuracion del modeulo Patchauto
  $word_ignore = 'a, an, as, at, before, but, by, for, from, is, in, into, like, of, off, on, onto, per, since, than, the, this, that, to, up, via, with';
  variable_set('pathauto_ignore_words', $word_ignore);
  variable_set('pathauto_transliterate', TRUE);

  variable_set('boost_root_cache_dir', 'sites/default/files/boost/cache');

  variable_set('features_default_export_path', 'profiles/craft/modules/features');

  //less configuration
  variable_set('less_autoprefixer', false);
  variable_set('less_devel', 1);
  variable_set('less_engine', 'less.php');
  variable_set('less_source_maps', 1);
  variable_set('less_watch', 1);

}

/**
 * Variables to theme.
 */
function _craft_variables_theme() {
  // Enable some standard blocks.
  $configure = [
    'toggle_logo' => 1,
    'toggle_name' => 0,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 0,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'bootstrap__active_tab' => '',
    'bootstrap_fluid_container' => 1,
    'bootstrap_button_size' => '',
    'bootstrap_button_colorize' => 1,
    'bootstrap_button_iconize' => 1,
    'bootstrap_forms_required_has_error' => 0,
    'bootstrap_forms_smart_descriptions' => 1,
    'bootstrap_forms_smart_descriptions_limit' => '250',
    'bootstrap_forms_smart_descriptions_allowed_tags' => 'b, code, em, i, kbd, span, strong',
    'bootstrap_image_shape' => '',
    'bootstrap_image_responsive' => 1,
    'bootstrap_table_bordered' => 0,
    'bootstrap_table_condensed' => 0,
    'bootstrap_table_hover' => 1,
    'bootstrap_table_striped' => 1,
    'bootstrap_table_responsive' => '-1',
    'bootstrap_breadcrumb' => '1',
    'bootstrap_breadcrumb_home' => 1,
    'bootstrap_breadcrumb_title' => 1,
    'bootstrap_navbar_position' => '',
    'bootstrap_navbar_inverse' => 0,
    'bootstrap_pager_first_and_last' => 1,
    'bootstrap_region_well-navigation' => '',
    'bootstrap_region_well-header' => '',
    'bootstrap_region_well-highlighted' => '',
    'bootstrap_region_well-help' => '',
    'bootstrap_region_well-content' => '',
    'bootstrap_region_well-sidebar_first' => '',
    'bootstrap_region_well-sidebar_second' => '',
    'bootstrap_region_well-footer' => '',
    'bootstrap_region_well-page_top' => '',
    'bootstrap_region_well-page_bottom' => '',
    'bootstrap_anchors_fix' => 0,
    'bootstrap_anchors_smooth_scrolling' => 1,
    'bootstrap_forms_has_error_value_toggle' => 1,
    'bootstrap_popover_enabled' => 1,
    'bootstrap_popover_animation' => 1,
    'bootstrap_popover_html' => 1,
    'bootstrap_popover_placement' => 'bottom',
    'bootstrap_popover_selector' => '',
    'bootstrap_popover_trigger' => [
      'click' => 'click',
      'hover' => 0,
      'focus' => 0,
      'manual' => 0,
    ],
    'bootstrap_popover_trigger_autoclose' => 1,
    'bootstrap_popover_title' => '',
    'bootstrap_popover_content' => '',
    'bootstrap_popover_delay' => '',
    'bootstrap_popover_container' => '',
    'bootstrap_tooltip_enabled' => 1,
    'bootstrap_tooltip_animation' => 1,
    'bootstrap_tooltip_html' => 1,
    'bootstrap_tooltip_placement' => 'bottom',
    'bootstrap_tooltip_selector' => '',
    'bootstrap_tooltip_trigger' => [
      'hover' => 'hover',
      'focus' => 'focus',
      'click' => 0,
      'manual' => 0,
    ],
    'bootstrap_tooltip_delay' => '',
    'bootstrap_tooltip_container' => '',
    'bootstrap_toggle_jquery_error' => 0,
    'bootstrap_cdn_provider' => '',
    'bootstrap_cdn_custom_css' => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.css',
    'bootstrap_cdn_custom_css_min' => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css',
    'bootstrap_cdn_custom_js' => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.js',
    'bootstrap_cdn_custom_js_min' => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js',
    'bootstrap_cdn_provider_import_data' => '',
    'bootstrap_cdn_jsdelivr_version' => '3.3.5',
    'bootstrap_cdn_jsdelivr_theme' => 'bootstrap',

  ];
  variable_set('theme_craft_central_settings', $configure);

  // Enable the admin theme.
  db_update('system')
    ->fields(['status' => 1])
    ->condition('type', 'theme')
    ->condition('name', 'seven')
    ->execute();

  db_update('system')
    ->fields(['status' => 1])
    ->condition('type', 'theme')
    ->condition('name', 'craft_central')
    ->execute();

  variable_set('admin_theme', 'seven');
}

/**
 * Variables Admin menu.
 */
function _craft_variables_admin_menu() {
  variable_set('admin_menu_cache_client', 0);
  variable_set('admin_menu_components', [
    'admin_menu.icon' => TRUE,
    'admin_menu.menu' => TRUE,
    'admin_menu.account' => TRUE,
    'shortcut.links' => TRUE,
    'admin_menu.search' => FALSE,
    'admin_menu.users' => FALSE,
  ]);
  variable_set('admin_menu_margin_top', 1);
  variable_set('admin_menu_position_fixed', 1);
  variable_set('admin_menu_tweak_modules', 0);
  variable_set('admin_menu_tweak_permissions', 0);
  variable_set('admin_menu_tweak_tabs', 0);
}

/**
 * Configure pathauto.
 */
function _craft_variables_pathauto() {
  //variable_set('pathauto_node_pattern', '[node:content-type]/[node:title]');
}
