<?php
/**
 * @file
 * craft_desing_maker_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function craft_desing_maker_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'admin_products';
  $view->description = 'Design Makers administre own products';
  $view->tag = 'default';
  $view->base_table = 'webform_submissions';
  $view->human_name = 'Admin Products';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Administer Products';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
  $handler->display->display_options['style_options']['columns'] = array(
    'value' => 'value',
    'value_1' => 'value_1',
    'value_2' => 'value_2',
    'edit_submission' => 'edit_submission',
    'delete_submission' => 'delete_submission',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'value' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_submission' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete_submission' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['bootstrap_styles'] = array(
    'bordered' => 'bordered',
    'hover' => 'hover',
    'condensed' => 'condensed',
    'striped' => 0,
  );
  $handler->display->display_options['style_options']['responsive'] = 1;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Not content submit yet!';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Webform submissions: User */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'webform_submissions';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['label'] = '';
  $handler->display->display_options['fields']['value']['webform_nid'] = '3';
  $handler->display->display_options['fields']['value']['webform_cid'] = '9';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['label'] = 'Image';
  $handler->display->display_options['fields']['value_1']['webform_nid'] = '3';
  $handler->display->display_options['fields']['value_1']['webform_cid'] = '10';
  /* Field: Webform submission data: Value */
  $handler->display->display_options['fields']['value_2']['id'] = 'value_2';
  $handler->display->display_options['fields']['value_2']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['value_2']['field'] = 'value';
  $handler->display->display_options['fields']['value_2']['label'] = '';
  $handler->display->display_options['fields']['value_2']['webform_nid'] = '3';
  $handler->display->display_options['fields']['value_2']['webform_cid'] = '11';
  /* Field: Webform submissions: Delete link */
  $handler->display->display_options['fields']['delete_submission']['id'] = 'delete_submission';
  $handler->display->display_options['fields']['delete_submission']['table'] = 'webform_submissions';
  $handler->display->display_options['fields']['delete_submission']['field'] = 'delete_submission';
  $handler->display->display_options['fields']['delete_submission']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['delete_submission']['access_check'] = 1;
  /* Contextual filter: Webform submissions: User */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'webform_submissions';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin-products';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'My Products';
  $handler->display->display_options['menu']['description'] = 'Manager my products';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['admin_products'] = $view;

  $view = new view();
  $view->name = 'designer_directory';
  $view->description = 'Designer maker directory';
  $view->tag = 'default';
  $view->base_table = 'civicrm_contact';
  $view->human_name = 'Designer directory';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Designer maker directory';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'views_bootstrap_thumbnail_plugin_style';
  $handler->display->display_options['style_options']['alignment'] = 'vertical';
  $handler->display->display_options['style_options']['columns_horizontal'] = '-1';
  $handler->display->display_options['style_options']['columns_vertical'] = '3';
  $handler->display->display_options['style_options']['columns_xs'] = '12';
  $handler->display->display_options['style_options']['columns_sm'] = '6';
  $handler->display->display_options['style_options']['columns_md'] = '4';
  $handler->display->display_options['style_options']['columns_lg'] = '3';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: CiviCRM Contacts: Drupal ID */
  $handler->display->display_options['relationships']['drupal_id']['id'] = 'drupal_id';
  $handler->display->display_options['relationships']['drupal_id']['table'] = 'civicrm_contact';
  $handler->display->display_options['relationships']['drupal_id']['field'] = 'drupal_id';
  /* Field: CiviCRM Contacts: Contact ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: CiviCRM Contacts: Contact Image */
  $handler->display->display_options['fields']['image_URL']['id'] = 'image_URL';
  $handler->display->display_options['fields']['image_URL']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['image_URL']['field'] = 'image_URL';
  $handler->display->display_options['fields']['image_URL']['label'] = '';
  $handler->display->display_options['fields']['image_URL']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['image_URL']['alter']['path'] = 'contacts/[id]';
  $handler->display->display_options['fields']['image_URL']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['image_URL']['url_only'] = 0;
  $handler->display->display_options['fields']['image_URL']['image_style'] = 'large';
  /* Field: CiviCRM Contacts: Display Name */
  $handler->display->display_options['fields']['display_name']['id'] = 'display_name';
  $handler->display->display_options['fields']['display_name']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['display_name']['field'] = 'display_name';
  $handler->display->display_options['fields']['display_name']['label'] = '';
  $handler->display->display_options['fields']['display_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['display_name']['alter']['path'] = 'contacts/[id]';
  $handler->display->display_options['fields']['display_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['display_name']['link_to_civicrm_contact'] = 0;
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['relationship'] = 'drupal_id';
  $handler->display->display_options['filters']['rid']['value'] = array(
    5 => '5',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'designer-maker-directory';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Designer maker directory';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['designer_directory'] = $view;

  $view = new view();
  $view->name = 'designer_maker_profile';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'civicrm_contact';
  $view->human_name = 'Designer maker profile';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Designer maker profile';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: CiviCRM Contacts: Contact ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: CiviCRM Contacts: Contact Image */
  $handler->display->display_options['fields']['image_URL']['id'] = 'image_URL';
  $handler->display->display_options['fields']['image_URL']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['image_URL']['field'] = 'image_URL';
  $handler->display->display_options['fields']['image_URL']['label'] = '';
  $handler->display->display_options['fields']['image_URL']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['image_URL']['url_only'] = 0;
  $handler->display->display_options['fields']['image_URL']['image_style'] = 'thumbnail';
  /* Field: CiviCRM Contacts: Display Name */
  $handler->display->display_options['fields']['display_name']['id'] = 'display_name';
  $handler->display->display_options['fields']['display_name']['table'] = 'civicrm_contact';
  $handler->display->display_options['fields']['display_name']['field'] = 'display_name';
  $handler->display->display_options['fields']['display_name']['link_to_civicrm_contact'] = 0;
  /* Field: CiviCRM Custom: Designer Maker Data: Accepts Commissions */
  $handler->display->display_options['fields']['accepts_commissions_6']['id'] = 'accepts_commissions_6';
  $handler->display->display_options['fields']['accepts_commissions_6']['table'] = 'civicrm_value_biography_3';
  $handler->display->display_options['fields']['accepts_commissions_6']['field'] = 'accepts_commissions_6';
  /* Field: CiviCRM Custom: Designer Maker Data: Biography */
  $handler->display->display_options['fields']['biography_4']['id'] = 'biography_4';
  $handler->display->display_options['fields']['biography_4']['table'] = 'civicrm_value_biography_3';
  $handler->display->display_options['fields']['biography_4']['field'] = 'biography_4';
  /* Field: CiviCRM Custom: Designer Maker Data: Discipline */
  $handler->display->display_options['fields']['discipline_5']['id'] = 'discipline_5';
  $handler->display->display_options['fields']['discipline_5']['table'] = 'civicrm_value_biography_3';
  $handler->display->display_options['fields']['discipline_5']['field'] = 'discipline_5';
  /* Field: CiviCRM Email: Email Address */
  $handler->display->display_options['fields']['email']['id'] = 'email';
  $handler->display->display_options['fields']['email']['table'] = 'civicrm_email';
  $handler->display->display_options['fields']['email']['field'] = 'email';
  $handler->display->display_options['fields']['email']['location_type'] = '0';
  $handler->display->display_options['fields']['email']['location_op'] = '0';
  $handler->display->display_options['fields']['email']['is_primary'] = 0;
  /* Contextual filter: CiviCRM Contacts: Contact ID */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'civicrm_contact';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'civicrm_id';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'contacts';
  $export['designer_maker_profile'] = $view;

  $view = new view();
  $view->name = 'portfolio';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'civicrm_contact';
  $view->human_name = 'Portfolio';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Portfolio';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_bootstrap_carousel_plugin_style';
  $handler->display->display_options['style_options']['interval'] = '5000';
  $handler->display->display_options['style_options']['navigation'] = 1;
  $handler->display->display_options['style_options']['indicators'] = 0;
  $handler->display->display_options['style_options']['pause'] = 1;
  $handler->display->display_options['row_plugin'] = 'views_bootstrap_thumbnail_plugin_rows';
  $handler->display->display_options['row_options']['image'] = 'producs_1';
  $handler->display->display_options['row_options']['title'] = 'product_title_2';
  $handler->display->display_options['row_options']['content'] = array(
    'description_3' => 'description_3',
    'producs_1' => 0,
    'product_title_2' => 0,
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Not found products for this desing maker.';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Field: CiviCRM Custom: Product: Description */
  $handler->display->display_options['fields']['description_3']['id'] = 'description_3';
  $handler->display->display_options['fields']['description_3']['table'] = 'civicrm_value_products_1';
  $handler->display->display_options['fields']['description_3']['field'] = 'description_3';
  $handler->display->display_options['fields']['description_3']['label'] = '';
  $handler->display->display_options['fields']['description_3']['element_label_colon'] = FALSE;
  /* Field: CiviCRM Custom: Product: Image */
  $handler->display->display_options['fields']['producs_1']['id'] = 'producs_1';
  $handler->display->display_options['fields']['producs_1']['table'] = 'civicrm_value_products_1';
  $handler->display->display_options['fields']['producs_1']['field'] = 'producs_1';
  $handler->display->display_options['fields']['producs_1']['label'] = '';
  $handler->display->display_options['fields']['producs_1']['alter']['text'] = '<img src="[producs_1] ">';
  $handler->display->display_options['fields']['producs_1']['element_label_colon'] = FALSE;
  /* Field: CiviCRM Custom: Product: Title */
  $handler->display->display_options['fields']['product_title_2']['id'] = 'product_title_2';
  $handler->display->display_options['fields']['product_title_2']['table'] = 'civicrm_value_products_1';
  $handler->display->display_options['fields']['product_title_2']['field'] = 'product_title_2';
  $handler->display->display_options['fields']['product_title_2']['label'] = '';
  $handler->display->display_options['fields']['product_title_2']['element_label_colon'] = FALSE;
  /* Contextual filter: CiviCRM Contacts: Contact ID */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'civicrm_contact';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['id']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['portfolio'] = $view;

  return $export;
}
