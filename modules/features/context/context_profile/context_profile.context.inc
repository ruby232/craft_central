<?php
/**
 * @file
 * context_profile.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function context_profile_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'profile';
  $context->description = 'Page design maker profile';
  $context->tag = 'desing maker';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contacts/*' => 'contacts/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-portfolio-block' => array(
          'module' => 'views',
          'delta' => 'portfolio-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page design maker profile');
  t('desing maker');
  $export['profile'] = $context;

  return $export;
}
